#pragma once

template <typename LHS, typename Eq, typename RHS>
class EquationArray
{
public:
    EquationArray( double * const f, const RHS & rhs ) :f(f), rhs(rhs) {}

    __device__ __host__
    void operator () ( const size_t i )
    {
        return Eq::apply( &f[i], rhs(i) );
    }

private:
    double * const f;
    const RHS rhs;
    //const Scheme & scheme; //error!
};

template <typename LHS, typename Eq>
class EquationArray<LHS,Eq,double>
{
public:
    EquationArray( double * const f, const double rhs ) :f(f), rhs(rhs) {}

    __device__ __host__
    void operator () ( const size_t i )
    {
        return Eq::apply( &f[i], rhs );
    }

private:
    double * const f;
    const double rhs;
};

struct Substitution
{
    __device__ __host__
    static
    void apply ( double * const f, const double rhs )
    {
        *f = rhs;
    }
};

struct AdditionSubstitution
{
    __device__ __host__
    static
    void apply ( double * const f, const double rhs )
    {
        *f += rhs;
    }
};

struct SubtractionSubstitution
{
    __device__ __host__
    static
    void apply ( double * const f, const double rhs )
    {
        *f -= rhs;
    }
};

struct MultiplicationSubstitution
{
    __device__ __host__
    static
    void apply ( double * const f, const double rhs )
    {
        *f *= rhs;
    }
};

struct DivisionSubstitution
{
    __device__ __host__
    static
    void apply ( double * const f, const double rhs )
    {
        *f /= rhs;
    }
};

template <typename RHS>
class SubstituteArray
{
public:
    SubstituteArray( double * const f, const RHS rhs )
        : f(f), rhs(rhs) {}

    __device__ __host__
    void operator () ( const size_t i )
    {
        f[i] = rhs(i);
    }

private:
    double * const f;
    const RHS rhs;
};

class LHS_Array
{
public:
    LHS_Array( double * const array ) : array( array ) {}

    template<typename RHS>
    EquationArray<LHS_Array,Substitution,RHS>
    operator = ( const RHS rhs )
    {
        return EquationArray<LHS_Array,Substitution,RHS>( array, rhs );
    }

    template<typename RHS>
    EquationArray<LHS_Array,AdditionSubstitution,RHS>
    operator += ( const RHS rhs )
    {
        return EquationArray<LHS_Array,AdditionSubstitution,RHS>( array, rhs );
    }

    template<typename RHS>
    EquationArray<LHS_Array,SubtractionSubstitution,RHS>
    operator -= ( const RHS rhs )
    {
        return EquationArray<LHS_Array,SubtractionSubstitution,RHS>( array, rhs );
    }

    template<typename RHS>
    EquationArray<LHS_Array,MultiplicationSubstitution,RHS>
    operator *= ( const RHS rhs )
    {
        return EquationArray<LHS_Array,MultiplicationSubstitution,RHS>(array, rhs);
    }

    template<typename RHS>
    EquationArray<LHS_Array,DivisionSubstitution,RHS>
    operator /= ( const RHS rhs )
    {
        return EquationArray<LHS_Array,DivisionSubstitution,RHS>(array, rhs);
    }


private:
    double * const array;
};

