#pragma once

class AddMember
{
public:

    AddMember( const double * const a, const double * const b )
        : a(a), b(b) {}

    __device__ __host__
    double operator () ( const size_t i ) const
    {
        return a[i] + b[i];
    }

private:
    const double * const a;
    const double * const b;
};

class MulMember
{
public:

    MulMember( const double * const a, const double * const b )
        : a(a), b(b) {}

    __device__ __host__
    double operator () ( const size_t i ) const
    {
        return a[i] * b[i];
    }

private:
    const double * const a;
    const double * const b;
};
