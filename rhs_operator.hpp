#pragma once

struct Addition
{
    __host__ __device__
    static
    double apply( const double t0, const double t1 )
    {
        return t0 + t1;
    }
};

struct Subtraction
{
    __host__ __device__
    static
    double apply( const double t0, const double t1 )
    {
        return t0 - t1;
    }
};

struct Multiplication
{
    __host__ __device__
    static
    double apply( const double t0, const double t1 )
    {
        return t0 * t1;
    }
};

struct Division
{
    __host__ __device__
    static
    double apply( const double t0, const double t1 )
    {
        return t0 / t1;
    }
};

template <typename T0, typename Op, typename T1>
class Expression
{
public:
    Expression( const T0 term0, const T1 term1 )
        : term0( term0 ), term1( term1 ) {}

    __host__ __device__
    double operator () ( const size_t i ) const
    {
        return Op::apply( term0(i), term1(i) );
    }

    Expression<double,Multiplication,Expression<T0,Op,T1> >
    operator - (void)
    {
        return Expression<double,Multiplication,Expression<T0,Op,T1> >(-1.0,*this);
    }

private:
    const T0 term0;
    const T1 term1;
};

template <typename Op, typename T1>
class Expression<double, Op, T1>
{
public:
    Expression( const double term0, const T1 term1 )
        : term0( term0 ), term1( term1 ) {}

    __host__ __device__
    double operator () ( const size_t i ) const
    {
        return Op::apply( term0, term1(i) );
    }

    Expression<double,Multiplication,Expression<double,Op,T1> >
    operator - (void)
    {
        return Expression<double,Multiplication,Expression<double,Op,T1> >(-1.0,*this);
    }

private:
    const double term0;
    const T1     term1;
};

template <typename T0, typename Op>
class Expression<T0, Op, double>
{
public:
    Expression( const T0 term0, const double term1 )
        : term0( term0 ), term1( term1 ) {}

    __host__ __device__
    double operator () ( const size_t i ) const
    {
        return Op::apply( term0(i), term1 );
    }

    Expression<double,Multiplication,Expression<T0,Op,double> >
    operator - (void)
    {
        return Expression<double,Multiplication,Expression<T0,Op,double> >(-1.0,*this);
    }

private:
    const T0     term0;
    const double term1;
};
