#include <iostream>
#include "foreach.hpp"
#include "lhs.hpp"
#include "rhs.hpp"
#include "rhs_operator_function.hpp"
#include "rhs_basic.hpp"

int test2()
{
    const int n = 10;

    double * const   a  = new double[n];
    double * const   b  = new double[n];

    double * const out0 = new double[n];
    double * const out1 = new double[n];
    double * const out2 = new double[n];

    for ( int i = 0 ; i < n ; i++ ) {
        a[i] = i;
        b[i] = i*i;
        out0[i] = 1000.0;
        out1[i] = 1000.0;
        out2[i] = 1000.0;
    }

    double * d_a;
    double * d_b;
    double * d_out0;
    double * d_out1;

    cudaMalloc((void**) &d_a  , n * sizeof(double));
    cudaMalloc((void**) &d_b  , n * sizeof(double));

    cudaMalloc((void**) &d_out0, n * sizeof(double));
    cudaMalloc((void**) &d_out1, n * sizeof(double));

    cudaMemcpy( d_a   ,    a, n * sizeof(double), cudaMemcpyHostToDevice );
    cudaMemcpy( d_b   ,    b, n * sizeof(double), cudaMemcpyHostToDevice );
    cudaMemcpy( d_out0, out0, n * sizeof(double), cudaMemcpyHostToDevice );
    cudaMemcpy( d_out1, out1, n * sizeof(double), cudaMemcpyHostToDevice );

    //element_wise_operation<<<1, n>>>(d_out, d_a, d_b, Add());
    //element_wise_operation<<<1, n>>>(d_out, d_a, d_b, Mul());
    //for_each<<<1,n>>>( d_out, AddMember(d_a, d_b));

    //for_each<<<1,n>>>( d_out0, ArrayValue(d_a) * ArrayValue(d_b) );
    //const double ten = 10.0;

    for_each_void<<<1,n>>>
        //( LHS_Array(d_out0) = ConstantValue(1.0) * ArrayValue(d_a) * ArrayValue(d_b) );
        ( LHS_Array(d_out0) =  - ArrayValue(d_a) * ArrayValue(d_b) * - 1.0 );
        //( LHS_Array(d_out0) = ten );

    for_each_void<<<1,n>>>( SubstituteArray<MulMember>( d_out1, MulMember(d_a, d_b) ) );

    cudaMemcpy( out0, d_out0, n * sizeof(double), cudaMemcpyDeviceToHost );
    cudaMemcpy( out1, d_out1, n * sizeof(double), cudaMemcpyDeviceToHost );

    SubstituteArray<MulMember> func = SubstituteArray<MulMember>( out2, MulMember(a, b) );
    for ( int i = 0 ; i < n ; i++ ) {
        func(i);
    }

    for ( int i = 0 ; i < n ; i++ ) {
        //printf( "a = %e\tb = %e\tout = %e\tans = %e\n", a[i], b[i], out[i], a[i]+b[i] );
        //printf( "a = %e\tb = %e\tout = %e\tans = %e\n", a[i], b[i], out[i], a[i]*b[i] );
        printf( "%f\t%f\t%f\n", out0[i], out1[i], out2[i] );
    }

    return 0;
}

int main()
{
    //return test0();
    //return test1();
    return test2();
}
