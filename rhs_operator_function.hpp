#pragma once
#include "rhs_operator.hpp"

template <typename T0, typename T1>
Expression<T0, Addition, T1>
operator + ( const T0 term0, const T1 term1 )
{
    return Expression<T0, Addition, T1>( term0, term1 );
}

template <typename T0, typename T1>
Expression<T0, Subtraction, T1>
operator - ( const T0 term0, const T1 term1 )
{
    return Expression<T0, Subtraction, T1>( term0, term1 );
}

template <typename T0, typename T1>
Expression<T0, Multiplication, T1>
operator * ( const T0 term0, const T1 term1 )
{
    return Expression<T0, Multiplication, T1>( term0, term1 );
}

template <typename T0, typename T1>
Expression<T0, Division, T1>
operator / ( const T0 term0, const T1 term1 )
{
    return Expression<T0, Division, T1>( term0, term1 );
}
