#pragma once

/*
class ConstantValue
{
public:

    ConstantValue( const double value )
        : value( value ) {}

    __device__ __host__
    double operator () ( const size_t i ) const
    {
        return value;
    }

private:
    const double value;
};
*/

class ArrayValue
{
public:

    ArrayValue( const double * const a )
        : a( a ) {}

    __device__ __host__
    double operator () ( const size_t i ) const
    {
        return a[i];
    }

    Expression<double,Multiplication,ArrayValue>
    operator - (void)
    {
        return Expression<double,Multiplication,ArrayValue>(-1.0,*this);
    }

private:
    const double * const a;
};
