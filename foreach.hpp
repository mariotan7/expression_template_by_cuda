#pragma once

template <typename Func>
__global__
void element_wise_operation
(
    double * const out,
    const double * const a,
    const double * const b,
    Func func
){
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    out[i] = func(a[i], b[i]);
}

template <typename Func>
__global__
void for_each
(
    double * const out,
    Func func
){
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    out[i] = func(i);
}

template <typename Func>
__global__
void for_each_void
(
    Func func
){
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    func(i);
}
