
CC   = gcc
CXX  = g++
GCC  = gcc
NVCC = nvcc

RM  = rm -f
MAKEDEPEND = makedepend

USEGPU = 1
USEGPUDOUBLE = 0

#CUDA_SDK_PATH = ${HOME}/NVIDIA_GPU_Computing_SDK
CUDA_PATH = /usr/local/cuda


CFLAGS    = -Wall -O3
CXXFLAGS  = $(CFLAGS)
NVCCFLAGS = -O3
LDFLAGS   = 

SRCS    = 
GPUSRCS = main.cu


TARGET = run
DISTTARGET = $(TARGET)_1.0.0

OBJS := $(filter %.o,$(SRCS:%.c=%.o))
OBJS += $(filter %.o,$(SRCS:%.cc=%.o))
OBJS += $(filter %.o,$(SRCS:%.cpp=%.o))
OBJS += $(filter %.o,$(SRCS:%.cu=%.o))
GPUOBJS := $(filter %.o,$(GPUSRCS:%.cu=%.o))


ifeq ($(USEGPU),1)
    CFLAGS += -DGPU
    NVCCFLAGS +=  -DGPU -arch sm_20 -D__CUDA_ARCH__=200 -I$(CUDA_SDK_PATH)/C/common/inc
    OBJS += $(GPUOBJS)
    SRCS += $(GPUSRCS)
    LDFLAGS += -L$(CUDA_PATH)/lib64 -lcuda -lcudart #-L$(CUDA_SDK_PATH)/C/lib -lcutil_x86_64
    ifeq ($(USEGPUDOUBLE),1)
    else
        CFLAGS += -DGPU_FLOAT
        NVCCFLAGS += -DGPU_FLOAT
    endif
else
    NVCC = $(CXX) -x c++
    NVCCFLAGS = $(CFLAGS)
endif


DEPENDENCIES = $(subst .o,.d,$(OBJS))


.PHONY: all
all :
	$(MAKE) -j3 $(TARGET)

$(TARGET) : $(OBJS)
	$(CXX) $(CXXFLAGS) $(TARGET_ARCH) $(OBJS) -o $@ $(LDFLAGS)

%.o : %.c
	$(call make-depend,$<,$@,$(subst .o,.d,$@))
	$(CC) $(CFLAGS) $(TARGET_ARCH)-c $<

%.o : %.cc
	$(call make-depend,$<,$@,$(subst .o,.d,$@))
	$(CXX) $(CXXFLAGS) $(TARGET_ARCH) -c $<

%.o : %.cpp
	$(call make-depend,$<,$@,$(subst .o,.d,$@))
	$(CXX) $(CXXFLAGS) $(TARGET_ARCH) -c $<
%.o : %.cu
	$(call make-depend,-x c++ $<,$@,$(subst .o,.d,$@))
	$(NVCC) $(NVCCFLAGS) $(TARGET_ARCH) -c $<



.PHONY: dist
dist :
	mkdir -p $(DISTTARGET)
	@for h in `makedepend -Y -f- -- $(CXXFLAGS) -- $(SRCS) | grep -e ":" | sed -e "s/.*: //" | tr " " "\n" | sort | uniq` ; \
	do \
		cp -p $$h $(DISTTARGET); \
	done
	cp -p $(SRCS) $(DISTTARGET)
	cp -p Makefile $(DISTTARGET)
	tar -zcvf $(DISTTARGET).tar.gz $(DISTTARGET)
	rm -rf $(DISTTARGET)


.PHONY: clean
clean :
	$(RM) $(TARGET)
	$(RM) $(OBJS)
	$(RM) $(DEPENDENCIES)
	$(RM) *~



ifneq "$(MAKECMDGOALS)" "clean"
  -include $(DEPENDENCIES)
endif

# $(call make-depend,source-file,object-file,depend-file)
define make-depend
  @$(GCC) -MM            \
          -MF $3         \
          -MP            \
          -MT $2         \
          $(CFLAGS)      \
          $(CXXFLAGS)    \
          $(TARGET_ARCH) \
          $1
endef


