#pragma once

struct FunctionObject
{
    __device__
    float operator () (const float & x)
    {
        return 2.0 * x + 1.0;
    }
};

struct Add
{
    template <typename T>
    __device__
    T operator () ( const T a, const T b ) const
    {
        return a + b;
    }
};

struct Mul
{
    template <typename T>
    __device__
    T operator () ( const T a, const T b ) const
    {
        return a * b;
    }
};
